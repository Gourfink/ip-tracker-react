import React, { PureComponent } from "react";
import axios from 'axios';
import IpForms from "./components/IpForms.js";
import IpData from "./components/IpData.js";
import IpMap from "./components/IpMap.js";
import bg from "./img/pattern-bg.png";

import Container from '@mui/material/Container';
import 'leaflet/dist/leaflet.css';
import './App.css';

export default class App extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
        mapData: null,
    };
  }

    async componentDidMount() {
        const storage = await window.localStorage.getItem('mapData') ;
        if (storage) {
            this.state.mapData = JSON.parse(storage);
        }
    }

    /**
     * Send a request to the ipcheck API and populate State with the response
     * @param {*} data 
     * @returns void
     */
    handleSearchChange = async(data) => {
      if (!data) return;
        // Prepare the request
        const requestData = {
            ip: data,
        };
        const request = {
            method: 'GET',
            headers: {
                'Authorization': 'Bearer WookieIP2022',
                'Content-Type': 'application/json',
            },
            data: JSON.stringify(requestData),
            url: `https://wookie.codesubmit.io/ipcheck?ip=${data}`,
        };
        try {
            // axios request
            const mapData = await axios(request);

            // save to localStorage
            window.localStorage.setItem('mapData', JSON.stringify(mapData.data));
            this.setState({ mapData: mapData.data });
        } catch(error) {
            // display errors in console
            console.log(`There was an error ${error.message}`)
        }
    };

  render() {
    return (
        <div>
           <div className="top-container" style={{background: 'url('+ bg +')', backgroundSize: 'cover'}}>
                    <Container>
                        <IpForms
                            ipData={this.state.mapData}
                            searchIpRequest={this.handleSearchChange}
                        />
                    </Container>
                </div>
                <div className="map-container">
                    <Container>
                        <IpData ipData={this.state.mapData} />
                    </Container>
                    <Container maxWidth={false} disableGutters>
                        <IpMap ipData={this.state.mapData} />
                    </Container>
                </div>
        </div>
    );
  }
}
