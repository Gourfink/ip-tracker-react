import React, { PureComponent } from "react";
import { Grid } from '@mui/material';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';

export default class IpData extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            ipData: props.ipData,
        };
    }

    componentWillReceiveProps(props) {
        this.setState({ ipData: props.ipData });
        this.render();
    }
    render() {
        return (
            <Card className="form data-container" elevation={0}>
                <CardContent>
                    <Grid container spacing={2} px={2} pt={2}>
                        <Grid xs={12} md={3} textAlign="center">
                            <Typography variant="h5">IP ADDRESS</Typography>
                            <p>{ this.state.ipData?.ip || '' }</p>
                        </Grid>
                        <Grid xs={12} md={3} textAlign="center">
                        <Typography variant="h5">LOCATION</Typography>
                            <p>
                                { this.state.ipData?.location?.city || '' } 
                                { this.state.ipData?.location?.postalCode || '' } 
                                { this.state.ipData?.location?.region || '' } 
                                { this.state.ipData?.location?.country || '' }
                            </p>
                        </Grid>
                        <Grid xs={12} md={3} textAlign="center">
                            <Typography variant="h5">TIMEZONE</Typography>
                            <p>{ this.state.ipData?.location.timezone || '' }</p>
                        </Grid>
                        <Grid xs={12} md={3} textAlign="center">
                            <Typography variant="h5">ISP</Typography>
                            <p>{ this.state.ipData?.isp || '' }</p>
                        </Grid>
                    </Grid>
                </CardContent>
            </Card>
        );
    }
}
