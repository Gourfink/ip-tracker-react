import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import TextField from '@mui/material/TextField';
import ChevronRight from '@mui/icons-material/ChevronRight';
import { spacing } from '@mui/system';

export default class IpForms extends PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            ip: null,
        };
    }

    static propTypes = {
        ipData: PropTypes.object,
        searchIpRequest: PropTypes.func
    };

    handleChange = event => {
        this.state.ip = event.target.value;
    };

    searchIp = event => {
        this.props.searchIpRequest(this.state.ip);
    };
  render() {
    return (
        <Card
            className="form"
            elevation={0}
            style={{backgroundColor: 'transparent', textAlign: 'center'}}
        >
            <h2 className="my-8" style={{color: '#ffffff'}}>
                IP Address Tracker
            </h2>
            <CardContent style={{ maxWidth: '600px', margin: 'auto'}}>
                <TextField
                    v-cy-helper="{ element:'ip-field' }"
                    placeholder=" Search for any IP address or domain"
                    height="55"
                    style={{
                        backgroundColor: '#FFFFFF',
                        borderRadius: '15px 0px 0px 15px',
                        width: '400px'
                }}
                    className="ip-input"
                    onChange={this.handleChange}
                    value={this.ip}
                    sx={{ mx: 'auto' }}
                />
                <ChevronRight
                    style={{
                        color: '#FFFFFF',
                        backgroundColor: '#000000',
                        borderRadius: '0px 15px 15px 0px',
                        padding: '15px',
                        cursor: 'pointer'
                }}
                    size={25}
                    onClick={this.searchIp}
                />
            </CardContent>
        </Card>
    );
  }
}
