import React, { PureComponent } from "react";
import ChangeView from "./MapComponents/ChangeView.js";

import { latLng } from "leaflet";
import { MapContainer, TileLayer, Marker } from 'react-leaflet';

export default class IpMap extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            ipData: props.ipData,
            position: latLng(props.ipData?.location?.lat, props.ipData?.location?.lng) || latLng(51.505, -0.09),
        };
    }

    componentWillReceiveProps(props) {
        this.setState({ ipData: props.ipData });
        this.setState({
            position: latLng(props.ipData?.location?.lat, props.ipData?.location?.lng) || latLng(51.505, -0.09)
        });
        this.render();
    }

    map({ center, zoom }) {
        return (
            <MapContainer style={{height: '500px'}} center={center} zoom={zoom} scrollWheelZoom={false} zIndex={-1}>
                <ChangeView center={center} zoom={zoom} />
                <TileLayer
                    attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                <Marker position={center} />
            </MapContainer>
        );
    }

    render() {
        return this.map({ center: this.state.position, zoom: 13 });
    }
}
